package recursion;

public interface AlgorithmInterface
{

	public String Go(String input);
	public String getDescription();
	public String getSourceWithComments();
	
	
}
