package recursion;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author Eric
 */
public class RecImp_Eric implements AlgorithmInterface {
    private String srcCode = "public String go(String s) {\n"
            + "\tif (s.isEmpty()) { // 1st base case.\n"
            + "\t\treturn \"\";\n"
            + "\t} else if (s.length() == 1) { // 2nd base case.\n"
            + "\t\treturn \"\"+s.charAt(0);\n"
            + "\t} else { // General (recursive) case.\n"
            + "\t\treturn \"\"+s.charAt(0) + go(s.substring(2));\n"
            + "\t}\n"
            + "}";
    
    public String Go(String s) {
        if (s.isEmpty()) { // 1st base case.
            return "";
        } else if (s.length() == 1) { // 2nd base case.
            return ""+s.charAt(0);
        } else { // General (recursive) case.
            return ""+s.charAt(0) + Go(s.substring(2));
        }
    }
    
    public String getDescription() {
        return "This method takes a string argument and returns a string with every second character of the passed string missing.";
    }
    
    public String getSourceWithComments() {
        return srcCode;
    }
}
