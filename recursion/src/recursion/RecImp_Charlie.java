/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recursion;

/**
 *
 * @author Charlie
 */
public class RecImp_Charlie implements AlgorithmInterface{
    private String srcCode = "public String go(String s) {\n"
            + "\t int n = Integer.parseInt(input); // Parser den til en int\n"
            + "\t\tint result=fibonacci(n); //Her kalder man metoden og referere den til en int\n"
            + "\t\tif(number < 1){\n" 
            + "\t\t throw new IllegalArgumentException(\"Invalid argument for Fibonacci series: + number);} //Her kalder man metoden og referere den til en int\n"
            + "\t\tif(number == 1 || number == 2) return 1;  }// Base case of recursion\n"
            + "\t\treturn fibonacci(number-2) + fibonacci(number -1);//recursive method call\n"
            + "\t\tString realresult= Integer.toString(result);//Her laver man integeren om til en string\n"
            + "\t\treturn realresult;//Her returnes resultat\n"
            + "\t}\n"
            + "}";   
    
   public static int fibonacci(int number){
        if(number < 1){
            throw new IllegalArgumentException("Invalid argument for Fibonacci series: "
                                                + number);
        }
        //base case of recursion
        if(number == 1 || number == 2){
            return 1;
        }
        //recursive method call in java
        return fibonacci(number-2) + fibonacci(number -1);
    }
   

    @Override
    public String Go(String input) {
       int n = Integer.parseInt(input);
    int result=fibonacci(n);
    String realresult= Integer.toString(result);
    return realresult;
    }
    
    @Override
    public String getDescription() {
        return "Denne metode laver om på rækkefølgen af fibonacci";
    }

    @Override
    public String getSourceWithComments() {
       return srcCode;
    }
}
