/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recursion;

/**
 *
 * @author mikkel
 */
public class RecImp_Mikkel implements AlgorithmInterface{
    
    static long count = 0;
    static String string = "";
    /*public static void main(String args[])
    {
        System.out.println(getSourceWithComments());
    }*/

    public void towersOfHanoi(int discs, String source, String auxiliary, String destination)
    {
   // move (discs-1) discs from source to aux(mid peg) using destination peg as the auxiliary/intermidiate peg
   // move bottom disk from peg source to peg destination
   // move (discs-1)discs from peg aux(mid peg) to peg destination while using source peg as auxiliary/intermediate peg
        
    // static int count is to see how many moves is needed to complete this shit
    count++;
    // this test only moves the top disc. the top disc must be moved between every other move. (assume top disk is multiple discs if discs > 2)
    if(discs == 1)
     {
         string +="Top disc from " + source + " to " + destination+"\n";
     }
     else
     {
         // this call pretty much moves all the discs (-1) to the specified destination peg, using the auxiliary to move the discs. 
         towersOfHanoi(discs-1, source, destination, auxiliary);
         // this prints the other disc's start and the destination. a top disc must be moved after this.
         string +="Disc number " + discs + " from " + source + " to " + destination+"\n";
         // this call moves the rest of the discs from the auxiliary, where the discs are now located, to the destination peg, using the auxiliary peg.
         towersOfHanoi(discs-1, auxiliary, source, destination);
     }
    }
    
    
    @Override
    public String getSourceWithComments()
    {
        return " public void towersOfHanoi(int disc, String source, String auxiliary, String destination)\n" +
"    {\n" +
"   \n" +
"        \n" +
"    // static int count is to see how many moves is needed to complete this shit\n" +
"    count++;\n" +
"    // this test only moves the top disc. the top disc must be moved between every other move\n" +
"    if(disc == 1)\n" +
"     {\n" +
"         string +=\"Top disc from \" + source + \" to \" + destination+\"\\n\";\n" +
"     }\n" +
"     else\n" +
"     {\n" +
"         // this call pretty much moves all the discs (-1) to the specified destination peg, using the auxiliary to move the discs. \n" +
"         towersOfHanoi(disc-1, source, destination, auxiliary);\n" +
"         // this prints the other disc's start and the destination. a top disc must be moved after this.\n" +
"         string +=\"Disc number \" + disc + \" from \" + source + \" to \" + destination+\"\\n\";\n" +
"         // this call moves the rest of the discs from the auxiliary, where the discs are now located, to the destination peg, using the auxiliary peg.\n" +
"         towersOfHanoi(disc-1, auxiliary, source, destination);\n" +
"     }\n" +
"    }";
    }
    @Override
    public String getDescription()
    {
        return "call the go() method and enter a number as a parameter. The number parametised is the "
                + "amount of discs on the first tower."
                + "\nA call go(3) would mean the first tower has 3 discs for a start.";
    }

    @Override
    public String Go(String input) 
    {
        int discs = Integer.parseInt(input);
        towersOfHanoi(discs, "First", "Second", "Third");
        return "Number of moves: "+count + "\n"+ string;
    }
  }


