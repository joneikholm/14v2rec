package recursion;

public class RecImp_Pelle implements AlgorithmInterface {

    public static String s = "foo";
    

    public static String repeatString(int n) {
    
        if(n < 0) {
            throw new IllegalArgumentException();
        }
        if(n == 0) {
            return "";
        } else {
        
            return s += repeatString(n-1);
        }
    }
    @Override
    public String Go(String input) {
        return repeatString(Integer.parseInt(input));
    }

    @Override
    public String getDescription() {
        return "Method repeatString takes an integer n and returns a string \"foo\" n times";
    }

    @Override
    public String getSourceWithComments() {
        
        return "public static String repeatString(int n) {\n" +
"    \n" +
"        if(n < 0) {\n" +
"            throw new IllegalArgumentException();\n" +
"        }\n" +
"        if(n == 0) {\n" +
"            return \"\";\n" +
"        } else {\n" +
"        \n" +
"            return s += repeatString(n-1);\n" +
"        }\n" +
"    }";
    }


}