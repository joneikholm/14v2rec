/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recursion;

/**
 *
 * @author Delonge
 */
public class RecImp_Pelle2 implements AlgorithmInterface{

    private static String string = "";
    

    //Takes an Integer has parameter and write the sequence of numbers
    public static void writeSequence(int n) {
    
        if(n == 1) {
        
            string += n;
        } else if (n == 2) {
            
            string += n-1;
            writeSequence(n-1);
        } else {
        
            string +=(1+n)/2;
            writeSequence(n-2);
            string +=(1+n)/2;
        }
    }
    @Override
    public String Go(String input) {
        string = "";
        writeSequence(Integer.parseInt(input));
        return string;
    }

    @Override
    public String getDescription() {
        
        return "TTakes an Integer has parameter and write a sequence of numbers\n" +
"if the integer is odd 1 '1' will occur in the middle \n" +
"if even 2 '1' will occur";
    }

    @Override
    public String getSourceWithComments() {
        
        return "public static void writeSequence(int n) {\n" +
"    \n" +
"        if(n == 1) {\n" +
"        \n" +
"            string += n;\n" +
"        } else if (n == 2) {\n" +
"            \n" +
"            string += n-1;\n" +
"            writeSequence(n-1);\n" +
"        } else {\n" +
"        \n" +
"            string +=(1+n)/2;\n" +
"            writeSequence(n-2);\n" +
"            string +=(1+n)/2;\n" +
"        }\n" +
"    }";
    }

    
}
